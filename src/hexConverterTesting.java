public class hexConverterTesting {
    public static void main(String[] args) {
        int testData = 50;
        String expectedOutput = "0x32";
        String myOutput = hexConverterFunction.decToHexFunction(testData);
        System.out.println("50 in hex is: " + expectedOutput);
        System.out.println("My code outputs: " + myOutput);
    }
}
