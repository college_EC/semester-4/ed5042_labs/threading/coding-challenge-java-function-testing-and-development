/*
* Name: Eoghan Conlon
* ID:   21310262
* This class is a helper class to both of the activities that I have in this Coding Challenge as both
* activities use decimal to binary converter. The BinaryCalculator uses it to get its output while the
* HexCalculator uses it as its first step in conversion.
*
* This file was in a college submission repo, I have it here for use in the dec to hex function.
 */

public class binaryConverterHelper {

    private static int check = 0;
    public static String converter(int bits, int input){
        long max;
        String rValue = "";
        //Binary numbers go to (2^bits) -1
        max = (long) (Math.pow(2, bits) - 1);
        if(input > max){
            rValue = "ERROR, number too big";
        } else {
            int length = 0;
            for(int i = bits - 1; i >= 0; i--){
                if (converterHelp(input, i)){
                    check += (int)  Math.pow(2, i);
                    rValue += "1";
                } else {
                    rValue += "0";
                }
                length += 1;
                if(length % 4 == 0){
                    rValue += " ";
                }
            }
        }
        return rValue;
    }

    private static boolean converterHelp(int input, int bit){
        return (input >= ((int)  (Math.pow(2, bit)) + check));
    }
}
