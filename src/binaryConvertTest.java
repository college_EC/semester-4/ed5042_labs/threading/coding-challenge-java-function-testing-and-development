public class binaryConvertTest {
    public static void main(String[] args) {
        int[] bitsTested = {4, 8};
        int input = (int) (Math.random()  * bitsTested[(bitsTested.length - 1)]);
        for (int j : bitsTested) {
            System.out.println(input + " converted to binary with " + j + " bits is " + binaryConverterHelper.converter(j, input));
        }
    }
}
