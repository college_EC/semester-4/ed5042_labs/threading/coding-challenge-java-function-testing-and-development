public class hexConverterFunction {
    public static String decToHexFunction(int input){
        String rValue = "0x";
        boolean error;
        String inputAsBinary = binaryConverterHelper.converter(8, input);

        String[] inputAsBinary4Blocks = inputAsBinary.split(" ");

        error = false;
        for(String s: inputAsBinary4Blocks){
            switch (s) {
                case "0000":{
                    rValue += "0";
                }
                case "0001":{
                    rValue += "1";
                }
                case "0010":{
                    rValue += "2";
                }
                case "0011":{
                    rValue += "3";
                }
                case "0100":{
                    rValue += "4";
                }
                case "0101":{
                    rValue += "5";
                }
                case "0110":{
                    rValue += "6";
                }
                case "0111":{
                    rValue += "7";
                }
                case "1000":{
                    rValue += "8";
                }
                case "1001":{
                    rValue += "9";
                }
                case "1010":{
                    rValue += "A";
                }
                case "1011":{
                    rValue += "B";
                }
                case "1100":{
                    rValue += "C";
                }
                case "1101":{
                    rValue += "D";
                }
                case "1110":{
                    rValue += "E";
                }
                case "1111":{
                    rValue += "F";
                }
                default:{
                    error = true;
                }
            }
            if(error){
                rValue = "If you are seeing this, congrats you have somehow managed to break a function that I have already tested.";
                break;
            }
        }
        return rValue;
    }
}
